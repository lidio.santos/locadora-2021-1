package builder;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.ucsal.bes20211.testequalidade.locadora.dominio.Cliente;
import br.ucsal.bes20211.testequalidade.locadora.dominio.Locacao;
import br.ucsal.bes20211.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20211.testequalidade.locadora.dominio.Modelo;
public class LocacaoBuilder {
	
	private static final Integer DEFAULT_seq = 0;
	private Integer seq=DEFAULT_seq;
	
	private static final Integer DEFAULT_numeroContrato=1;
	private Integer numeroContrato=DEFAULT_numeroContrato;
	
	//private static final Cliente DEFAULT_cliente("00100200345", "Teste", "719919919191");

	private List<Veiculo> veiculos;

	private static final Date DEFAULT_dataLocacao=null;
	private Date dataLocacao=DEFAULT_dataLocacao;

	
	private static final Integer DEFAULT_quantidadeDiasLocacao=5;
	private Integer quantidadeDiasLocacao=DEFAULT_quantidadeDiasLocacao;

	
	private static final Date DEFAULT_dataDevolucao=null;
	private Date dataDevolucao=DEFAULT_dataDevolucao;
	
	private LocacaoBuilder() {
	
	}
	
	public static LocacaoBuilder umaLocacao() {
		return new LocacaoBuilder();
	}
	
	public LocacaoBuilder ComContrato(Integer numeroContrato) {
		this.numeroContrato=numeroContrato;
		return this;
	}
	
	public LocacaoBuilder Cliente(String cpf, String nome, String telefone) {
		//this.Cliente("cpf", "nome", "telefone")= cliente;
		return this;
		
	}
	
	public LocacaoBuilder mas() {
		//return new LocacaoBuilder().comContrato(numeroContrato).Cliente(cliente)
		return null;
	}
	
	public Locacao build() {
		Locacao locacao = new Locacao(null, veiculos, dataDevolucao, numeroContrato);
		
		return locacao;
	}
}
